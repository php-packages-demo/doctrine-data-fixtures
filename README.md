# doctrine/data-fixtures
* 333 2020-06

Data Fixtures for all Doctrine Object Managers [doctrine/data-fixtures](https://packagist.org/packages/doctrine/data-fixtures)

[![PHPPackages Rank](http://phppackages.org/p/doctrine/data-fixtures/badge/rank.svg)](http://phppackages.org/p/doctrine/data-fixtures)
[![PHPPackages Referenced By](http://phppackages.org/p/doctrine/data-fixtures/badge/referenced-by.svg)](http://phppackages.org/p/doctrine/data-fixtures)
http://www.doctrine-project.org

# Package with similar functionality
## nelmio/alice
* 433 2020-06

Expressive fixtures generator [nelmio/alice](https://packagist.org/packages/nelmio/alice)

[![PHPPackages Rank](http://phppackages.org/p/nelmio/alice/badge/rank.svg)](http://phppackages.org/p/nelmio/alice)
[![PHPPackages Referenced By](http://phppackages.org/p/nelmio/alice/badge/referenced-by.svg)](http://phppackages.org/p/nelmio/alice)

# Unofficial Documentation
* [*Fixtures, the right gestures*
  ](https://blog.theodo.fr/2019/04/fixtures-right-gestures/)
  2019  Victor Lebrun
* [*How to manage fixtures in a PHP project*
  ](https://blog.theodo.fr/2013/08/managing-fixtures/)
  2013 Benjamin Grandfond
* (fr) [*Améliorer la DX de vos Fixtures PHP*
  ](https://jolicode.com/blog/ameliorer-la-dx-de-vos-fixtures-php)
  2020-06 Grégoire Pineau